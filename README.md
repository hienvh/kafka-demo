1. Install docker
2. Install docker-compose
3. ```docker-compose up -d```
4. Create topic 
```node admin```
5. Consume message: tạo bao nhiêu consume tùy ý, message sẽ được delivery theo chiến thuật round robin. 
Mỗi terminal sẽ là 1 consumer instance
```node consumer```
6. Start producer to produce message
```node producer```
Producer sẽ nhận message từ stdin. Chỉ việc gõ và enter , hoặc copy pase, mỗi dòng sẽ là 1 message.